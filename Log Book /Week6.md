# __IDP Report Week 6__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|2/12/2021|

### Agenda & Goals 
+ The sprayer with design and structure gondola team have a meeting to discuss on how to bind and attach the sprayer to the bottom of the gondola. 
+ There is some of the propose idea oh how to attach the payload to the gondola for example is by using a slot and the other idea was to use the clamp. 
+ The gondola team have a meeting with Mr. Fikri and present our progress on the gondola design. 

### Decisions 
+ After some disccussion with the sprayer team we decided to use the clamp method in order to bind the sprayer to the bottom layer of the gondola. 
+ Nut and bolt is been used to fastened the clamp to the gondola. 
+ Mr. Fikri also asked us to change the gondola design from three layers to only two layers where the battery will share the same space with the esc and the other layer is for the carrier board. 
+ Small compartment at the top layer also being design to put the speed indicator.

### Method & Justification 
+ Below is the new asembly design of the gondola design after some adjustment been made. 
<img src="Image/gondola_assemble__crop.jpeg" width=400 align=middle>

+ The sprayer design that we receive from the payload subsystem. 
<img src="Image/payload.jpeg" width=400 align=middle>

+ We will install 2 pairs of clamp at the bottom layer of gondola which is at the front and back that will be used as the attachment for the sprayer. 

### Impact 
+ The reason why we used four clamp where two is in front and the other two at the back becaause this will make the structure strong and not shaking while flying. 
+ Next, by deciding to only make two layers instead of three because this will contribute to a more lighter gondola and a better space management. 
+ The clamp is fastened to the gondola by a nut and bolt because it is more stronger and easy to use than when we use the screw. 

### Next Step 
+ We need to discuss on how to attach the gondola and the sprayer to the airship ballon. 
+ Meeting with Mr. Fikri to finalise our design and start the 3D print of our product. 
+ Find and buy some material that will be used at the nearby store. 
