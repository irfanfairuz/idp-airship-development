# __IDP Report Week 9__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|23/12/2021|

### Agenda & Goals 
+ Complete the assembly of the gondola.
+ To install the latch on every sides of the gondola. 

### Decisions 
+ The gondola team member decided to only using latch as the attachment of both bottom and top layer of the gondola. 
+ Latch is chosen due to to its simple procedure to install as well as this latch is strong enough to hold gondola in place. 

### Method & Justification 
+ The figure below shown the latch chosen to be used on the gondola.

<img src="Image/image_2022-02-05_204901.png" width=400 align=middle>

+ The image of the gondola after the latch installed. 

<img src="Image/latch_installed.jpeg" width=400 align=middle>


### Impact 
+ The gondola is now attached together and is ready to insert all the control system component. 

### Next Step 
+ The sprayer team have made a small adjustment where the gondola position is now on top of the sprayer tank.
+ The gondola need to be rescale and made it into a smaller size, following the tank dimension so that the gondola can fit perfectly on top of the tank. 
