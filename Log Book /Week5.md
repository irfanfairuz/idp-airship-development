# __IDP Report Week 5__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|25/11/2021|

### Agenda & Goals 
+ In last week class, after the discussion with Mr. Azizi and Mr. Fikri, we are asked to change our gondola tray mechanism for every layer because if we proceed with this design, it will be hard for the control system and integration team to do their configuration on the airship. 
+ Due to this, our team conducting a meeting within ourself to find another method to solve this issues. 
+ We also decide to use a combination of 3D printing and mppf material in contructing our gondola. 

### Decisions 
+ After the meeting, our team decided to use a latch to attach the layers (3 layers) for our gondola instead of using a sliding tray system. 
+ Mppf material is choosen because it is very light and easy to shape. Thus, by only using the 3D print material for the frame and the structure, and use mppf for its wall, we can reduce the total weight of the gondola. 

### Method & Justification 
+ Below is the new gondola design after some adjustment been made.
+ The blue segment on the sketch is actually the wall of the gondola that we decide to use the mppf material. Thus from the outside, people will not see the inner part of the gondola.  
<img src="Image/week_5.jpeg" width=400 align=middle>

### Impact 
+ The gondola become easier to access because we change the tray method and using latch on each side of the gondola. 


### Next Step 
+ Will conduct a meetiing with the sprayer team to discuss on how to attach the payload with the gondola. 

