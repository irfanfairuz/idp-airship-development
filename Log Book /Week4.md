# __IDP Report Week 4__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|18/11/2021|

### Agenda & Goals 
+ For this week, my group which is the gondola team, are having a meeting and discussion to decide and finalise the gondola design for the airship. 
+ Some of our group members also go to the faculty laboratory this week to meet with Mr. Fikri and directly see the current gondola design and all of its component. 

### Decisions 
+ For the design, we agreed to create it with a rectangular shape. 
+ And for the compartment inside the gondola we build it with a three layer stages. The bottom layer is for the battery, middle layer for esc and lastly for the carrier board. 


### Method & Justification 
+ We design the three layer system by using a tray mechanism, so that we can just slide the tray in and out easily. 
+ Below is the complete sketch of the gondola design. 
<img src="Image/WhatsApp_Image_2021-11-11_at_11.08.18_PM.jpeg" width=450 align=middle>

### Impact 
+ We propose an idea to only use 3D printing for the main frame and structure (including the tray holder) of the gondola and use the mppf for most of the walls in order to make the gondola lighter. 
+ By installing the lightening holes around the gondola body also contributing in making the total weight of the gondola reduced. 
 
### Next Step 
+ Need to change our main idea by using the tray and sliding mechanism for our gondola design, because Mr Azizi mentioned that if we used this method, it will be hard when we want to setting and setup the control system for our airship. 
