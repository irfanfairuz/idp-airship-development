# __IDP Report Week 13__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|20/1/2022|

### Agenda & Goals 
+ Proceed with applying the putty layer on the gondola body. 
+ Spray paint the gondola.
+ Installed the latch on sides of the gondola.  

### Decisions 
+ The putty layer and the spray paint is applied as a finishing. 
+ We used the puttty sprayer that available on market instead of wood filler because it is much easier. 
+ And spray paint the gondola body black. 

### Method & Justification 
+ The funtion of applying the putty layer is to fills the cracks, imperfections, and gaps in the wood to create an even base for the paint. 
+ The figure shown below is the final gondola looks after the putty layer is applied, as well as the spray paint and the attachment of the latch on the gondola body. 

<img src="Image/final_final_product.jpeg" width=400 align=middle>


### Impact 
+ The gondola looks more aesthetically pleasing. 
+ The gondola surface is smooth. 

### Next Step 
+ Do the checklist for the gondola team for the test flight later.
+ Assisting the other subsytem that need helps. 
+ Ready to be attached on the airship body. 
