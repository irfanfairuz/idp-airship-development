# __IDP Report Week 8__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|16/12/2021|

### Agenda & Goals 
+ Supposedly start the printing process of the gondola design by using the 3D printer machine.
+ The gondola team member decided to change the material of the gondola from using the 3D printing to laser cutter machine.
+ The cutting process as well as the assembly of the gondola by parts is being done. 

### Decisions 
+ Gondola team decided to change the material to plywood instead of continue using the 3D printing because it the process take about 4 days to complete the printing. 
+ The by parts of the gondola that being cut by the machine is been assembled and attached by using a hot glue gun. 

### Method & Justification 
+ The decision made to change the material to plywood is because of time restriction due to the scheduled first flight of the airship on friday of 8th week and the printing procces took so long to complete. 
+ The gondola by parts is being developed and the process the setting for the cutting procees is been set by using a software called RD Works. 

+ The first figure shown below is how our gondola is cut by using the laser cutter machine.

<img src="Image/laser_cut_process.jpeg" width=400 align=middle>

+ The second figure is the bottom layer of the gondola after the assembly. 

<img src="Image/bottom_layer_laser_cutter.jpeg" width=400 align=middle>

+ The third figure is the product after the assembly. 

<img src="Image/product_laser_cutter.jpeg" width=389 align=middle>

### Impact 
+ The time taken to complete the process is greatly reduced.
+ The gondola can be used to put all the component for the first flight test.

### Next Step 
+ To install the latch on every side of the gondola body as an attachment mechanism in between the bottom layer and the second layer of the gondola. 
