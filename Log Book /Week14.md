# __IDP Report Week 14__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|27/1/2022|

### Agenda & Goals 
+ Attach the gondola to the airship body. 
+ Decided on how to attach the gondola to the airship ballon.

### Decisions 
+ String and valcro is choosen as an attachment of the gondola to the airship ballon. 

### Method & Justification 
+ The image below is the valcro applied on of airship ballon. 

<img src="Image/valcro.jpeg" width=400 align=middle>

+ The procces on setting up the string to the airship ballon 

<img src="Image/string.jpeg" width=400 align=middle>

### Impact 
+ The gondola will not moving while flying. 
+ The string and valcro is strong enough to hold the gondola. 

### Next Step 
+ Fly the airship.
