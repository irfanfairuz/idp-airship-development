# __IDP Report Week 12__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|13/1/2022|

### Agenda & Goals 
+ Proceed with the cutting procces of the gondola part. 
+ Completing the final design of the gondola. 
+ Decided on what type of glue neeed to be used to replace the hot glue for the attachment of the whole gondola body. 

### Decisions 
+ The same procedure is applied where as the material used is plywood and the cutting procces is done using the laser cutter machine. 
+ The gondola team decided to use epoxy adhesive glue due to the concern of the hot glue will melt while flying resulting from the hot surrounding temperature. 

### Method & Justification 
+ Epoxy adhesive glue is chosen because epoxies have high corrosion resistance and are less affected by water and heat.
+ Below is the epoxy adhesive glue used for the gondola.
  
<img src="Image/epoxy_adhesive.jpg" width=400 align=middle>

+ The final design of the gondola including the sleeves for ESC. 

<img src="Image/final_design_rescale.jpeg" width=400 align=middle>


### Impact 
+ The structure of the gondola become stronger. 
+ ESC position is secured. 

### Next Step 
+ Applied a putty layer on the surface of the gondola.
+ Spray paint the gondola body.
+ Installed latch on the sides of the gondola. 
