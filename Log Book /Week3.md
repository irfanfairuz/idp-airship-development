# __IDP Report Week 3__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|11/11/2021|

### Agenda & Goals 
+ For this week, we have conducted a meeting with all of the members for the gondola team from the design and structure system. 
+ The main goals is to determine the material we decide to use to develop our gondola. 

### Decisions 
+ During the discussion, our group members have propose some material to use as a structure for our gondala. For example is using the 3D print and other option is to use the balsa wood. 
+ All of the option above is been analyse and observe. 

### Method & Justification 
+ The reason to use balsa wood is because of it is lighter than the 3D print material. As an example, the density of the balsa wood also is far more lower than the 3D print filament.
+ But it is proven that the 3D print material is much better than the other option and the only disadvantage of this material is heavier. 

### Impact 
+ There is a certain problem with the use of balsa wood when we use it as the main material for our gondola. Firstly, is regarding the durability and strength toward bad wheather like rain. The balsa wood may be damage when it is wet. so we reach a conclusion to not using the balsa wood as our material to build the gondola.
+ 3D is stronger and have a great sustainbility than the other selected material. 
 
### Next Step 
+ We all agreed to use the 3D print as our main material for our gondola design.
+ Brainstorming among our team to find a way to reduce the total weight for our gondola design. (construct a lightening holes around the gondola) 
+ Another way is to find other material to use together with the 3D print when constructing the gondola design to reduce more weight. 
+ Conducted a meeting with Mr. Fikri at the faculty laboratory to discuss and the see the current gondola design. 

