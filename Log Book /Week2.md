# __IDP Report Week 2__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|4/11/2021|

### Agenda & Goals 
+ According to our subsystem group that have been assigned by Dr. Salah, we have our own seperate meeting.  
+ In the meeting we discussed among ourself about the problem and task we need to do in the design and structure group. 
+ On 2/11/2021, our group conducted a meeting with Mr. Fikri to ask and get more information on what we need to do for the project especially with the thruster arm and gondola design. 

### Decisions 
+ After the meeting with all of our members, we have discussed some of the task that our subsystem need to do and complete it. 
+ The problem is listed and shown on the table below;

|Problem|Explanation|
|---|---|
|Thruster Arm|Need to design a new thruster arm that are more stable then the current design, and need to manufacture a bigger arm that are suitable with the new design.|
|Gondola|Need to design and develop a new gondola. (lighter)|
|Distributor Board Cover|This part need to further investigate and discuss with the control system team. Especially regarding its size and shape.|
|Sprayer Mechanism|Need a further discussion with the  sprayer design subsystem in order to develop a mechanism used to attach the sparayer to the new airship.|

### Method & Justification 
+ After all of the problem have been listed, we develop a Ghantt Chart for this project so that we have a clear understanding of the task that need to be done.
+ Since the thruster arm and the gondola is the crucial part for the first milestone, we divided our group into 2 smaller team that have been assigned to do this two task for the first milestone.
+ From the meeting we conduct together with Mr Fikri, we identify the work that we need to do about the thruster arm and the gondola. As example, for gondola, we need to make sure that it is lighter than the current design. This is because, with lighter gondola we can carry more payload with the airship and in this project our payload is the sprayer. Next, we also need to manufacture the gondala so that they consist of three layers which is for the distributor board, ESC, and battery. 
+ The next thing that we need to do is regarding the thruster arm. For this part, we need to design a new thruster arm that are more strong and stable then the current design. This is because, the older one tends to vibrate when the propeller is on due to this, the data obtained from the airship is not accurate and unreliable. The thruster arm team must think a new approach to tackle this problem. 

### Impact 
+ From the meeting and the discussion that have been done, all of our member are now understand the idea and task for our subsystem.
+ By developing the Ghantt Chart also make sure our team are able to planning and schedulling our task for the project smoothly and efficiently. 
+ From the meeting also, we practice our ability to work in a group and get to know our members in details.

### Next Step 
+ Planning on how to develop a good design of thruster arm and gondola
+ Discussing among our group members regarding the material that we want to use for our thruster arm and  gondola desingn so that they are stronger and at the same time lighter. 
+ Installing a CAD software which is SolidWorks because Mr. Fikri asked as to use this software for designing our product. 
