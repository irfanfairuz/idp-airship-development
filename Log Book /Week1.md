# __IDP Report Week 1__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|28/10/2021|

### Agenda & Goals 
+ On the first week of the class, Dr. Salah introduced us to this course which is the Aerospace Design Project (ADP). 
+ We received all the information regarding this subject and at the same time project that we need to accomplish thorught out this semester. 
+ The project that my coursemates and I need to do is to develop, design and manufature an airship with a sprayer that will be utilized by the agriculutural industry. 
+ We also have been divided into eight subsystem for the project and another smaller group of teams. 
+ On 26/10/2021 a workshop on how to use GitLab have been conducted with the helps from Mr. Azizi for us. 

### Decisions 
+ In deciding the team member, we are required to complete a survey by Dr. Salah and also fill in the google form. Where the google form ask us to choose and rate all of the eight subsystem from what we prefer the most and vice versa. 
+ Below is the complete subsystem groups and the team members. 
<img src="Image/substym_n_team.jpeg" width=400 align=middle>

### Method & Justification 
+ We are being asked to fill in a survey on the google, that will be use by Dr. Salah to form a team and the subsystem for every students. 

### Impact 
+ By separting into smaller subsystem and teams, the workload will be shared and easy to commit to this project.
+ The Gitlab workshop that have been conducted that day helped students to understand more on how to use the Gitlab.
### Next Step 
+ We started the discussion related to the airship development project with our subsystem and also team members.


