# __IDP Report Week 7__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|2/12/2021|

### Agenda & Goals 
+ Finalise design for the gondola  
+ Creating a mount to place the carrier board in the top layer of the gondola. 

### Decisions 
+ Some minor adjustment have been made especially in the bottom layer of the gondola which is we change the bottom layer into the V-shape so that it looks more like a product just like Mr. Fikri mentioned. 
+ The design of the mount is created based on the perimeter of the carier board that will be used for the airship. 
+ we add some vent around the gondola to improve ventilation. 

### Method & Justification 
+ Below is the final design for our gondola and the mounting for the carrier board located at top layer of the gondola.
+ We also add a fillet feature on some of the edges at the gondola design. 
+ All of the design stated is made from CAD software which is the SolidWorks
<img src="Image/Final_design.png" width=400 align=middle>
<img src="Image/mount.png" width=389 align=middle>

### Impact 
+ By installing the vent around the gondola, the ventilation and heat management for the gondola is enhanced. 
+ The edges on the gondola design is been fillet because it will distributes the stress over a broader radius and at the same time preventing from deformation.

### Next Step 
+ We will start to 3D print the gondola. 
