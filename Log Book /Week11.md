# __IDP Report Week 11__

|Item|Detail|
|---|---|
|**Name**|Muhammad Arif Irfan Bin Muhammad Fairuz|
|**Matric No.**|198348|
|**Subsystem**|Design & Structure|
|**Date**|6/1/2022|

### Agenda & Goals 
+ Rescale the gondola full dimension referring to the sprayer tank dimension. 
+ After the measuring process, the gondola drawing was been adjusted by using SolidWorks. 
+ Plan on how to put the ESC outside the gondola. The position is change after the discussion with the FSI and Control system subsytem. 
+ To installed sleeve for the ESC on sides of the top layer. 

### Decisions 
+ The gondola need to be smaller in size because  the new position of the gondola have been changed which is on top of the sprayer tank.
+ The ESC sleeve is added in order to enhanced the safety of the ESC when flying. 
### Method & Justification 
+ The figure below show the latest position of the gondola. 

<img src="Image/new_location_of_the_gondola.jpeg" width=400 align=middle>

+ The measuring process of the new gondola size. 

<img src="Image/rescale_process.jpeg" width=400 align=middle>


### Impact 
+ The new gondola is now perfectly fit on top of the sprayer tank.
+ The sleeves for ESC added on side of the gondola will secured the ESC from moving when cruising. 

### Next Step 
+ Laser cut the gondola part.
+ Complete the assembly of the gondola. 
