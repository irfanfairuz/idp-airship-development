# __IDP 2021/2022: Airship Development__

__Name:__ [Muhammad Arif Irfan Bin Muhammad Fairuz](https://www.linkedin.com/in/muhammadarifirfan/)

__Matric No:__ 198348

__Course Name:__ Aerospace Design Project

__Course Code:__ EAS 4947

__Project Name:__ Airship Design for Agricultural Industry

__Subsystem:__ Design & Structure

## __Introduction__

## _My Profile_
<img src="https://gitlab.com/irfanfairuz/introduction/-/raw/main/passport%20photo/Passport_Photo.jpg" width=150 align=middle>

##
+ _Name:_ Muhammad Arif Irfan Bin Muhammad Fairuz
+ _Age:_ 22 Years old
+ _Course:_ Bachelor of Aerospace Engineering
+ _Staying at:_ Nilai, Negeri Sembilan
#
|strength|weakness|
|--------|--------|
|Flexible  |Self Critism|
|Honest|Too detail oriented|
|Creative|Introverted|


## Weekly Logbook 
- [Week 1](Log Book /Week1.md)
- [Week 2](Log Book /Week2.md)
- [Week 3](Log Book /Week3.md)
- [Week 4](Log Book /Week4.md)
- [Week 5](Log Book /Week5.md)
- [Week 6](Log Book /Week6.md)
- [Week 7](Log Book /Week7.md)
- [Week 8](Log Book /Week8.md)
- [Week 9](Log Book /Week9.md)
- Week 10
- [Week 11](Log Book /Week11.md)
- [Week 12](Log Book /Week12.md)
- [Week 13](Log Book /Week13.md)
- [Week 14](Log Book /Week14.md)
